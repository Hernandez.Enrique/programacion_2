package labs.labtwo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;


/**
 * LabTwoTest.
 */
public class LabTwoTest {
  @Test
  void testClients() {
    LabTwo labTwo = new LabTwo();
    Clients client = labTwo.addClient("John", ClientType.INDIVIDUAL, 1);
    assertEquals("John", client.getName(), "client should have a name");
    assertEquals(ClientType.INDIVIDUAL, client.getType(), "client should have a type");
    assertEquals(1, client.getId(), "client should have an id");
  }

  @Test
  void testMerchandise() {
    LabTwo labTwo = new LabTwo();
    Merchandise merchandise = labTwo.addMerchandise("Merch", MerchandiseType.ELECTRONICS, 1, 1, 1);
    assertEquals("Merch", merchandise.getName(), "merchandise should have a name");
    assertEquals(MerchandiseType.ELECTRONICS, merchandise.getType(), 
        "merchandise should have a type");
    assertEquals(1, merchandise.getId(), "merchandise should have an id");
    assertEquals(1, merchandise.getWeight(), "merchandise should have a weight");
    assertEquals(1, merchandise.getVolume(), "merchandise should have a volume");
  }

  @Test
  void testShipment() {
    LabTwo labTwo = new LabTwo();
    Clients client = labTwo.addClient("John", ClientType.INDIVIDUAL, 1);
    ArrayList<Merchandise> merchandiseList = new ArrayList<Merchandise>();
    merchandiseList.add(labTwo.addMerchandise("Merch", MerchandiseType.ELECTRONICS, 1, 1, 1));
    merchandiseList.add(labTwo.addMerchandise("Merch", MerchandiseType.FOOD, 2, 1, 1));
    Shipments shipment = labTwo.addShipment("1", ShipmentType.AIR, Priority.HIGH, "cdmx",
        1, merchandiseList, client);
    assertEquals("1", shipment.getId(), "shipment should have an id");
    assertEquals(ShipmentType.AIR, shipment.getType(), "shipment should have a type");
    assertEquals(Priority.HIGH, shipment.getPriority(), "shipment should have a priority");
    assertEquals("cdmx", shipment.getOrigin(), "shipment should have an origin");
    assertEquals(1, shipment.getDestination(),
        "shipment should have a destination");
    assertEquals(merchandiseList, shipment.getMerchandise(), 
        "shipment should have a list of merchandise");
    assertEquals(client, shipment.getClient(), "shipment should have a client");
  }

  @Test
  void testCost1() {
    LabTwo labTwo = new LabTwo();
    Clients client = labTwo.addClient("John", ClientType.INDIVIDUAL, 1);
    ArrayList<Merchandise> merchandiseList = new ArrayList<Merchandise>();
    merchandiseList.add(labTwo.addMerchandise("Merch", MerchandiseType.ELECTRONICS, 1, 1, 1));
    merchandiseList.add(labTwo.addMerchandise("Merch", MerchandiseType.CLOTHING, 2, 1, 1));
    labTwo.addShipment("1", ShipmentType.GROUND, Priority.HIGH, "cdmx",
        1, merchandiseList, client);
    assertEquals(680, labTwo.calculateCost("1"), "shipment should have a cost");
  }

}
