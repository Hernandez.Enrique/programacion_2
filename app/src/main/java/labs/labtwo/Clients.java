package labs.labtwo;

/**
 * Clients.
 */
public class Clients extends Entity {
  private ClientType type;

  /**
   * Constructor.
   */
  public Clients(String name, ClientType type, int id) {
    super(name, id);
    this.type = type;
    
  }

  public ClientType getType() {
    return type;
  }
}
