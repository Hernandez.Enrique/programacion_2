package labs.labtwo;

/**
 * Abstract class for entities.
 */
public abstract class Entity {

  private String name;
  private int id;

  /**
   * Constructor.
   */
  public Entity(String name, int id) {
    this.name = name;
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public int getId() {
    return id;
  }
}
