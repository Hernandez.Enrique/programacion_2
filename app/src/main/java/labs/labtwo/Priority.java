package labs.labtwo;

/**
 * Enum for shipment priorities.
 */
enum Priority {
  HIGH,
  MEDIUM,
  LOW
}