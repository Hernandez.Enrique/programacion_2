package labs.labtwo;

/**
 * Enum for client types.
 */
public enum ClientType {
    INDIVIDUAL, COMPANY, GOVERNMENT
}