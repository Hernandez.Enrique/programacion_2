package labs.labtwo;

/**
 * Enum for shipment types.
 */
enum ShipmentType {
  AIR,
  GROUND,
  SEA
}
