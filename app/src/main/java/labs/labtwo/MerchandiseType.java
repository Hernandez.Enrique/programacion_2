package labs.labtwo;

/**
 * Enum for merchandise types.
 */
enum MerchandiseType {
  ELECTRONICS,
  CLOTHING,
  FOOD
}
