package labs.labtwo;



/**
 * Merchandise.
 */
public class Merchandise extends Entity {
  private MerchandiseType type;
  private int weight;
  private int volume;

  /**
   * Constructor.
   */
  public Merchandise(String name, MerchandiseType type, int id, int weight, int volume) {
    super(name, id);
    this.type = type;
    this.weight = weight;
    this.volume = volume;
  }

  public MerchandiseType getType() {
    return type;
  }

  public int getWeight() {
    return weight;
  }

  public int getVolume() {
    return volume;
  }  
}
