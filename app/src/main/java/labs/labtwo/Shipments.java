package labs.labtwo;

import java.util.ArrayList;



/**
 * Shipments.
 */
public class Shipments {
  private String id;
  private ShipmentType type;
  private Priority priority;
  private String origin;
  private int destination; // km
  private ArrayList<Merchandise> merchandise;
  private Clients client;

  /**
   * Constructor.
   */
  public Shipments(String id, ShipmentType type, Priority priority, String origin, int destination,
      ArrayList<Merchandise> merchandise,
      Clients client) {
    this.id = id;
    this.type = type;
    this.priority = priority;
    this.origin = origin;
    this.destination = destination;
    this.merchandise = merchandise;
    this.client = client;
  }

  public String getId() {
    return id;
  }

  public ShipmentType getType() {
    return type;
  }

  public Priority getPriority() {
    return priority;
  }

  public String getOrigin() {
    return origin;
  }

  public int getDestination() {
    return destination;
  }

  public ArrayList<Merchandise> getMerchandise() {
    return merchandise;
  }

  public Clients getClient() {
    return client;
  }  
}
