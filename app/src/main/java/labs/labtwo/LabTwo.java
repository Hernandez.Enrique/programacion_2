package labs.labtwo;

import java.util.ArrayList;

/**
 * LabTwo - Sistema de Logística para Naviera Mercante.
 */
public class LabTwo {
  private ArrayList<Clients> clients;
  private ArrayList<Merchandise> merchandise;
  private ArrayList<Shipments> shipments;

  /**
   * Constructor.
   */
  public LabTwo() {
    this.clients = new ArrayList<Clients>();
    this.merchandise = new ArrayList<Merchandise>();
    this.shipments = new ArrayList<Shipments>();
  }

  /**
   * addClient.
   */
  public Clients addClient(String name, ClientType type, int id) {
    Clients client = new Clients(name, type, id);
    clients.add(client);
    return client;
  }

  /**
   * addMerchandise.
   */
  public Merchandise addMerchandise(String name, MerchandiseType type, int id,
      int weight, int volume) {
    Merchandise merch = new Merchandise(name, type, id, weight, volume);
    merchandise.add(merch);
    return merch;
  }

  /**
   * addShipment.
   */
  public Shipments addShipment(String id, ShipmentType type, Priority priority,
      String origin, int destination,
      ArrayList<Merchandise> merchandise,
      Clients client) {
    Shipments shipment = new Shipments(id, type, priority, origin,
        destination, merchandise, client);
    shipments.add(shipment);
    return shipment;
  }

  public ArrayList<Clients> getClients() {
    return clients;
  }

  public ArrayList<Merchandise> getMerchandise() {
    return merchandise;
  }

  public ArrayList<Shipments> getShipments() {
    return shipments;
  }

  /**
   * calculateCost.
   */
  public int calculateCost(String id) {
    int cost = 0;
    for (Shipments shipment : shipments) {
      if (shipment.getId().equals(id)) {
        for (Merchandise merch : shipment.getMerchandise()) {
          cost += (merch.getWeight() * 10) + (merch.getVolume() * 30);
        }
        cost += shipment.getDestination() * 100;
        if (shipment.getPriority().equals(Priority.HIGH)) {
          cost += 500;
        }
      }
    }
    return cost;
  }

}