package labs.labfour;

import java.util.Comparator;

/**
 * Student.
 */
public class Student {
  private String name;
  private int grade;

  public Student(String name, int grade) {
    this.name = name;
    this.grade = grade;
  }

  public String getName() {
    return name;
  }

  public int getGrade() {
    return grade;
  }

  @Override
  public String toString() {
    return "{" + name + "," + grade + "}";
  }

  /**
   * ByGrade comparator.
   */
  public static Comparator<Student> byGrade() {
    return new Comparator<Student>() {
      @Override
      public int compare(Student s1, Student s2) {
        return s1.getGrade() - s2.getGrade();
      }
    };
  }

}
