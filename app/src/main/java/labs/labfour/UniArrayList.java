package labs.labfour;

import java.util.AbstractList;
import java.util.Comparator;

/**
 * UniArrayList.
 */
public class UniArrayList<T> extends AbstractList<T> implements Unique<T>, Sortable<T> {
  private T[] array;
  private int size;

  public UniArrayList() {
    this.array = (T[]) new Object[10];
    int size = 0;
  }

  /**
   * Constructor.
   */
  public UniArrayList(T[] array) {
    this.array = (T[]) new Object[array.length];
    for (int i = 0; i < array.length; i++) {
      this.array[i] = array[i];
    }
    this.size = array.length;
  }

  @Override
  public void unique() {
    for (int i = 0; i < size; i++) {
      for (int j = i + 1; j < size; j++) {
        if (array[i].toString().equals(array[j].toString())) {
          remove(j);
          j--;
        }
      }
    }
  }

  @Override
  public void sortBy(Comparator<T> comparator) {
    for (int i = 0; i < size; i++) {
      for (int j = i + 1; j < size; j++) {
        if (comparator.compare(array[i], array[j]) > 0) {
          T temp = array[i];
          array[i] = array[j];
          array[j] = temp;
        }
      }
    }
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public void sort() {
    //comparar con toString
    for (int i = 0; i < size; i++) {
      for (int j = i + 1; j < size; j++) {
        if (array[i].toString().compareTo(array[j].toString()) > 0) {
          T temp = array[i];
          array[i] = array[j];
          array[j] = temp;
        }
      }
    }
  }

  @Override
  public T get(int index) {
    return array[index];
  }

  @Override
  public void add(int index, T element) {
    if (size == array.length) {
      T[] temp = (T[]) new Object[array.length * 2];
      for (int i = 0; i < size; i++) {
        temp[i] = array[i];
      }
      array = temp;
    }
    for (int i = size; i > index; i--) {
      array[i] = array[i - 1];
    }
    array[index] = element;
    size++;
  }

  @Override
  public T remove(int index) {
    T removed = array[index];
    for (int i = index; i < size - 1; i++) {
      array[i] = array[i + 1];
    }
    size--;
    return removed;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("[");
    for (int i = 0; i < size; i++) {
      sb.append(array[i]);
      if (i < size - 1) {
        sb.append(", ");
      }
    }
    sb.append("]");
    return sb.toString();
  }

  @Override
  public T set(int index, T element) {
    T replaced = array[index];
    array[index] = element;
    return replaced;
  }
}